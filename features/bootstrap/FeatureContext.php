<?php

use Behat\Behat\Context\ClosuredContextInterface, Behat\Behat\Context\TranslatedContextInterface, Behat\Behat\Context\BehatContext, Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode, Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Features context.
 */
class FeatureContext extends BehatContext {
	/**
	 * Initializes context.
	 * Every scenario gets it's own context object.
	 *
	 * @param   array   $parameters     context parameters (set them up through behat.yml)
	 */
	public function __construct(array $parameters) {
		// Initialize your context here
	}

	//
	// Place your definition and hook methods here:
	//
	//    /**
	//     * @Given /^I have done something with "([^"]*)"$/
	//     */
	//    public function iHaveDoneSomethingWith($argument)
	//    {
	//        doSomethingWith($argument);
	//    }
	//
	
	private function getDir($dir){
		return dirname(__FILE__) . '/../../' . $dir;
	}
	
	/**
	 * @Given /^ディレクトリ「([^」]*)」を新しく作る$/u
	 */
	public function resetADirectory($dir){
		$dir = $this->getDir($dir);
		if(is_dir($dir)){
			exec('rm -rf ' . $dir);
		}
		mkdir($dir);
	}
	
	/** 
	 * @Given /^I am in a directory "([^"]*)"$/
	 * @Given /^ディレクトリ「([^」]*)」に移動する$/u
	 */
	public function iAmInADirectory($dir) {
		$dir = $this->getDir($dir);
		if (!file_exists($dir)) {
			mkdir($dir);
		}
		chdir($dir);
	}
 /**
  * @Given /^I have a file named "([^"]*)"$/
  * @Given /^ファイル「([^」]*)」がある$/u
  */
    public function iHaveAFileNamed($file)
    {
        touch($file);
    }

    /**
	 * @When /^I run "([^"]*)"$/
	 * @When /^「([^」]*)」を叩いたら$/u
	 */
    public function iRun($command)
    {
        exec($command, $output);
        $this->output = trim(implode("\n", $output));
    }

    /**
	 * @Then /^I should get:$/
	 * @Then /^このように表示される$/u
	 */
    public function iShouldGet(PyStringNode $string)
    {
        if ((string) $string !== $this->output) {
            throw new Exception(
                "Actual output is:\n" . $this->output
            );
        }
    }
	
	/**
     * @Given /^「(.*)」のページを開いている$/
     */
    public function yahoooooooooo($url)
    {
    	//var_dump($url);
        //$this->driver = new \Behat\Mink\Driver\Selenium2Driver('firefox', $url);
        $this->driver = new \Behat\Mink\Driver\GoutteDriver();
		$this->session = new \Behat\Mink\Session($this->driver);
		$this->session->start();
		$this->session->visit($url);
		
		// $cssSelector = new \Behat\Mink\Selector\CssSelector();
		// $handler = new \Behat\Mink\Selector\SelectorsHandler();
		// $handler->registerSelector('css', $cssSelector);
    }

    /**
     * @Given /^「([^」]+)」に「(.+)」と入力$/
     */
    public function srchtfdsajklfjdsaioxt($target, $input)
    {
    	//var_dump($target, $input);
		$page = $this->session->getPage();
		var_dump($page);
		//$field = $page->find('css', 'input#srchtxt');
		$field = $page->find('css', 'input');
		// //var_dump($field);
		echo $field->getAttribute('name');
		$field->setValue($input);
		echo $field->getValue();
		//var_dump($field->getValue());
        throw new PendingException();
    }

    /**
     * @Given /^「([^」]*)」をクリック$/
     */
    public function srchbtfdsajiopn()
    {
        throw new PendingException();
    }

    /**
     * @Given /^タイトルに「(.*)」を含む$/
     */
    public function vfdsaijop()
    {
        throw new PendingException();
    }
	
}
